import asyncio
import logging
import signal
import subprocess
import tempfile
import time
from typing import Any, List, Optional
import aiohttp
import portpicker
import shutil
import os.path
import os
from sc2.client import Client
from s2clientprotocol import common_pb2 as c_pb
from s2clientprotocol import sc2api_pb2 as sc_pb
from sc2.paths import Paths
from sc2.player import Bot, Computer, Human, Observer
from sc2.protocol import Protocol
from sc2.sc2process import SC2Process
from sc2.data import Status
import sys
import async_timeout
from sc2.data import CreateGameError, Result
from sc2.game_state import GameState
from sc2.portconfig import Portconfig
from sc2.protocol import ConnectionAlreadyClosed, ProtocolError
logger = logging.getLogger(__name__)


class kill_switch:
    _to_kill: List[Any] = []

    @classmethod
    def add(cls, value):
        logger.debug("kill_switch: Add switch")
        cls._to_kill.append(value)

    @classmethod
    def kill_all(cls):
        logger.info("kill_switch: Process cleanup")
        for p in cls._to_kill:
            p._clean()


class SC2Process:
    def __init__(
        self,
        host: str = "127.0.0.1",
        port: Optional[int] = None,
        fullscreen: bool = False,
        render: bool = True,
        sc2_version: str = None
        ,base_build=None
        ,data_version=None
    ) -> None:
        assert isinstance(host, str)
        assert isinstance(port, int) or port is None

        self._render = render
        self._fullscreen = fullscreen
        self._host = host
        if port is None:
            self._port = portpicker.pick_unused_port()
        else:
            self._port = port
        self._tmp_dir = tempfile.mkdtemp(prefix="SC2_")
        self._process = None
        self._session = None
        self._ws = None
        self._sc2_version = sc2_version
        self.base_build = base_build
        self.data_version = data_version
        

    async def __aenter__(self):
        kill_switch.add(self)

        def signal_handler(*args):
            # unused arguments: signal handling library expects all signal
            # callback handlers to accept two positional arguments
            kill_switch.kill_all()

        signal.signal(signal.SIGINT, signal_handler)

        try:
            self._process = self._launch()
            self._ws = await self._connect()
        except:
            await self._close_connection()
            self._clean()
            raise

        return Controller(self._ws, self)

    async def __aexit__(self, *args):
        kill_switch.kill_all()
        signal.signal(signal.SIGINT, signal.SIG_DFL)

    @property
    def ws_url(self):
        return f"ws://{self._host}:{self._port}/sc2api"

    @property
    def versions(self):
        """ Opens the versions.json file which origins from
        https://github.com/Blizzard/s2client-proto/blob/master/buildinfo/versions.json """
        return VERSIONS

    def find_data_hash(self, target_sc2_version: str):
        """ Returns the data hash from the matching version string. """
        version: dict
        for version in self.versions:
            if version["label"] == target_sc2_version:
                return version["data-hash"]

    def _launch(self):
        args = [
            str(os.path.join(Paths.BASE,'Versions',self.base_build,'SC2_x64')) if self.base_build else str(Paths.EXECUTABLE),
            "-listen",
            self._host,
            "-port",
            str(self._port),
            "-displayMode",
            "1" if self._fullscreen else "0",
            "-dataDir",
            str(Paths.BASE),
            "-tempDir",
            self._tmp_dir,

        ]
        if self._sc2_version is not None:

            def special_match(strg: str, search=re.compile(r"([0-9]+\.[0-9]+?\.?[0-9]+)").search):
                """ Test if string contains only numbers and dots, which is a valid version string. """
                return not bool(search(strg))

            valid_version_string = special_match(self._sc2_version)
            if valid_version_string:
                data_hash = self.find_data_hash(self._sc2_version)
                assert (
                    data_hash is not None
                ), f"StarCraft 2 Client version ({self._sc2_version}) was not found inside sc2/versions.py file. Please check your spelling or check the versions.py file."
                args.extend(["-dataVersion", data_hash])
            else:
                logger.warning(
                    f'The submitted version string in sc2.rungame() function call (sc2_version="{self._sc2_version}") does not match a normal version string. Running latest version instead.'
                )

        if self.data_version:
            args.extend(['-dataVersion',self.data_version])

        if self._render:
            args.extend(["-eglpath", "libEGL.so"])

        if logger.getEffectiveLevel() <= logging.DEBUG:
            args.append("-verbose")

        return subprocess.Popen(
            args,
            cwd=(str(Paths.CWD) if Paths.CWD else None),
            # , env=run_config.env
        )

    async def _connect(self):
        for i in range(60):
            if self._process is None:
                # The ._clean() was called, clearing the process
                logger.debug("Process cleanup complete, exit")
                sys.exit()

            await asyncio.sleep(1)
            try:
                self._session = aiohttp.ClientSession()
                ws = await self._session.ws_connect(self.ws_url, timeout=120)
                logger.debug("Websocket connection ready")
                return ws
            except aiohttp.client_exceptions.ClientConnectorError:
                await self._session.close()
                if i > 15:
                    logger.debug(
                        "Connection refused (startup not complete (yet))")

        logger.debug("Websocket connection to SC2 process timed out")
        raise TimeoutError("Websocket")

    async def _close_connection(self):
        logger.info("Closing connection...")

        if self._ws is not None:
            await self._ws.close()

        if self._session is not None:
            await self._session.close()

    def _clean(self):
        logger.info("Cleaning up...")

        if self._process is not None:
            if self._process.poll() is None:
                for _ in range(3):
                    self._process.terminate()
                    time.sleep(0.5)
                    if not self._process or self._process.poll() is not None:
                        break
                else:
                    self._process.kill()
                    self._process.wait()
                    logger.error("KILLED")

        if os.path.exists(self._tmp_dir):
            shutil.rmtree(self._tmp_dir)

        self._process = None
        self._ws = None
        logger.info("Cleanup complete")


class Controller(Protocol):
    def __init__(self, ws, process):
        super().__init__(ws)
        self.__process = process

    @property
    def running(self):
        return self.__process._process is not None

    async def start_replay(self, replay_path, realtime, observed_id=1):  # Added
        ifopts = sc_pb.InterfaceOptions(
            raw=True, score=True, show_cloaked=True, raw_affects_selection=False, raw_crop_to_playable_area=False
        )
        req = sc_pb.RequestStartReplay(
            replay_path=replay_path, observed_player_id=observed_id, options=ifopts, realtime=False)

        result = await self._execute(start_replay=req)

        print(result)
        return result


async def start_replay(server, replay_path, realtime, observed_id):
    await server.start_replay(replay_path, realtime, observed_id)
    return Client(server._ws)


async def _host_game(replay_path, ai, realtime, portconfig, base_build, data_version,observed_id):
    async with SC2Process(fullscreen=False, base_build=base_build, data_version=data_version) as server:
        await server.ping()

        client = await start_replay(server, replay_path, realtime, observed_id)
        result = await observe(client, ai, realtime)
        return True


async def observe(client, ai, realtime=False, player_id=0):
    ai._initialize_variables()

    game_data = await client.get_game_data()
    game_info = await client.get_game_info()
    client.game_step = 1
    # This game_data will become self._game_data in botAI
    ai._prepare_start(client, player_id, game_info,
                      game_data, realtime=realtime)
    state = await client.observation()
    # check game result every time we get the observation
    if client._game_result:
        await ai.on_end(client._game_result[player_id])
        return client._game_result[player_id]
    gs = GameState(state.observation)
    proto_game_info = await client._execute(game_info=sc_pb.RequestGameInfo())
    ai._prepare_step(gs, proto_game_info)
    ai._prepare_first_step()
    try:
        await ai.on_start()
    except Exception as e:
        logger.exception(f"AI on_start threw an error")
        logger.error(f"resigning due to previous error")
        await ai.on_end(Result.Defeat)
        return Result.Defeat

    iteration = 0
    while True:
        if iteration != 0:
            if realtime:
                # TODO: check what happens if a bot takes too long to respond, so that the requested game_loop might already be in the past
                state = await client.observation(gs.game_loop + client.game_step)
            else:
                state = await client.observation()
            # check game result every time we get the observation
            if client._game_result:
                try:
                    await ai.on_end(client._game_result[player_id])
                except TypeError as error:
                    # print(f"caught type error {error}")
                    # print(f"return {client._game_result[player_id]}")
                    return client._game_result[player_id]
                return client._game_result[player_id]
            gs = GameState(state.observation)
            logger.debug(f"Score: {gs.score.score}")

            proto_game_info = await client._execute(game_info=sc_pb.RequestGameInfo())
            ai._prepare_step(gs, proto_game_info)

        logger.debug(
            f"Running AI step, it={iteration} {gs.game_loop * 0.725 * (1 / 16):.2f}s")

        try:
            if realtime:
                # Issue event like unit created or unit destroyed
                await ai.issue_events()
                await ai.on_step(iteration)
                await ai._after_step()
            else:

                # Issue event like unit created or unit destroyed
                await ai.issue_events()
                await ai.on_step(iteration)
                await ai._after_step()

        except Exception as e:
            if isinstance(e, ProtocolError) and e.is_game_over_error:
                if realtime:
                    return None
                # result = client._game_result[player_id]
                # if result is None:
                #     logger.error("Game over, but no results gathered")
                #     raise
                await ai.on_end(Result.Victory)
                return None
            # NOTE: this message is caught by pytest suite
            logger.exception(f"AI step threw an error")  # DO NOT EDIT!
            logger.error(f"Error: {e}")
            logger.error(f"Resigning due to previous error")
            try:
                await ai.on_end(Result.Defeat)
            except TypeError as error:
                # print(f"caught type error {error}")
                # print(f"return {Result.Defeat}")
                return Result.Defeat
            return Result.Defeat

        logger.debug(f"Running AI step: done")

        if not realtime:
            if not client.in_game:  # Client left (resigned) the game
                await ai.on_end(Result.Victory)
                return Result.Victory

        await client.step()  # unindent one line to work in realtime

        iteration += 1


def run_replay(ai, replay_path, realtime, base_build, data_version,observed_id=0):

    portconfig = Portconfig()
    result = asyncio.get_event_loop().run_until_complete(
        _host_game(replay_path, ai, realtime, portconfig, base_build, data_version,observed_id))
    return result
