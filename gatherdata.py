import os

dirpath = os.getcwd()
replays = os.listdir('replays/')

oldreplay = None

for replay in replays:
    if oldreplay:
        if oldreplay != replay:
            print("Deleting " + dirpath + "/replays/" + oldreplay)
            os.remove(dirpath + "/replays/" + oldreplay)

    oldreplay = replay
    os.system("python3 ObserverBot.py -r " + dirpath + "/replays/" + replay)
