import numpy as np
import tensorflow as tf
import os

dirpath = os.getcwd()

def collect_data():
    if not os.path.exists(dirpath + '/datasets/numpy'):
        os.makedirs(dirpath + '/datasets/numpy')

    myrace = os.listdir(dirpath + '/data/')
    for race in myrace:
        maps = os.listdir(dirpath + '/data/' + race + '/')
        for map in maps:
            enemy_races = os.listdir(dirpath + '/data/' + race + '/' + map + '/')
            for enemy_race in enemy_races:
                gamestates = os.listdir(dirpath + '/data/' + race + '/' + map + '/' + enemy_race + '/')
                for gamestate in gamestates:
                    datafiles = os.listdir(dirpath + '/data/' + race + '/' + map + '/' + enemy_race + '/' + gamestate + '/')
                    np.savez(dirpath + '/datasets/numpy/' + race + '_' + map + '_' + enemy_race + '_' + gamestate + '.npz', datafiles)



#collect_data()