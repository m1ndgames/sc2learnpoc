import string
import sc2
import sc2autoobserver
import observerAI
import asyncio
import mpyq
import six
import json
from scipy.spatial import distance
from sc2.ids.unit_typeid import UnitTypeId
from sc2.dicts.unit_train_build_abilities import TRAIN_INFO
from sc2 import Race
from sc2.game_data import UnitTypeData, AbilityData
from sc2 import AbilityId
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.data import ActionResult, race_townhalls
import random
import numpy as np
from typing import Any, Dict, List, Optional, Set, Tuple, Union, TYPE_CHECKING
import os
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-r", "--replayfile", dest="replayfile", help="replay file")
args = parser.parse_args()


class ObserverBot(observerAI.BotAI):
    def __init__(self, observed_id, observed_map, observed_race, enemy_race):
        self._already_seen = []
        self.close_units = []
        self.observed_id = observed_id
        self.observed_map = observed_map
        self.observed_race = observed_race
        self.enemy_race = enemy_race
        self.enemy_units_structures = []
        self.already_observed = set()
        self.own_start_location = None
        self.my_race = None
        self.all_enemy_units = []
        self.mystructures = []

    async def on_unit_created(self, unit):
        if int(self.time) == 0:
            if unit.is_structure and unit.owner_id == self.observed_id:
                if unit.name == 'Nexus' or unit.name == 'CommandCenter' or unit.name == 'Hatchery':
                    print("Starting Position: " + str(unit.position))
        else:
            if unit.is_structure and unit.owner_id == self.observed_id:
                if unit.name != 'KD8Charge' and unit.name != 'CreepTumorQueen' and unit.name != 'Pylon' and unit.name != 'SupplyDepot' and unit.name != 'Assimilator' and unit.name != 'Refinery' and unit.name != 'Extractor' and unit.name != 'CreepTumor':
                    print("Created Structure: " + str(unit.name) + " on " + str(unit.position) + " at " + str(self.time_formatted))
                    self.mystructures.append(unit.type_id.value)

    async def on_step(self, iteration):
        if iteration == 0:
            self.client.game_step = 1

        if self.state.game_loop == int(22.4 * 120):
            print("Saving early Structures")

            directory = "data/" + self.observed_race + "/" + self.observed_map + "/" + self.enemy_race + "/early/"
            if not os.path.exists(directory):
                os.makedirs(directory)

            array = np.array(self.mystructures)
            randomstring = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(20))
            np.save(directory + randomstring + ".npy", array)

        if self.state.game_loop == int(22.4 * 300):
            print("Saving midgame Structures")

            directory = "data/" + self.observed_race + "/" + self.observed_map + "/" + self.enemy_race + "/midgame/"
            if not os.path.exists(directory):
                os.makedirs(directory)

            array = np.array(self.mystructures)
            randomstring = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(20))
            np.save(directory + randomstring + ".npy", array)

        if self.state.game_loop == int(22.4 * 600):
            print("Saving endgame Structures")

            directory = "data/" + self.observed_race + "/" + self.observed_map + "/" + self.enemy_race + "/endgame/"
            if not os.path.exists(directory):
                os.makedirs(directory)

            array = np.array(self.mystructures)
            randomstring = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(20))
            np.save(directory + randomstring + ".npy", array)

            # No need to keep it running...
            exit(0)


    async def on_end(self, game_result):
        print('On end reached')


def get_replay_data(replay_data):
    replay_io = six.BytesIO()
    replay_io.write(replay_data)
    replay_io.seek(0)
    archive = mpyq.MPQArchive(replay_io).extract()
    metadata = json.loads(archive[b"replay.gamemetadata.json"].decode("utf-8"))

    if metadata['Players'][0]['Result'] == 'Win':
        observed_id = 1
        observed_race = metadata['Players'][0]['SelectedRace']
        enemy_race = metadata['Players'][1]['SelectedRace']
    elif metadata['Players'][1]['Result'] == 'Win':
        observed_id = 2
        observed_race = metadata['Players'][1]['SelectedRace']
        enemy_race = metadata['Players'][0]['SelectedRace']
    else:
        print("No Winner - Exiting...")
        exit(0)

    return metadata['BaseBuild'], metadata['DataVersion'], observed_id, observed_race, metadata['MapName'], enemy_race


def main():
    replay_path = args.replayfile
    if not replay_path:
        print("Please supply a replayfile!")
        exit(1)

    with open(replay_path, "rb") as f:
        base_build, data_version, observed_id, observed_race, mapfile, enemy_race = get_replay_data(f.read())

    observed_map = mapfile.replace('.SC2Map', '')

    print("Replay: " + replay_path)
    print("Race: " + observed_race)
    print("Enemy Race: " + enemy_race)
    print("Map: " + observed_map)

    sc2autoobserver.run_replay(
        ObserverBot(observed_id, observed_map, observed_race, enemy_race), replay_path, realtime=False, base_build=base_build, data_version=data_version, observed_id=observed_id
    )


if __name__ == '__main__':
    main()

